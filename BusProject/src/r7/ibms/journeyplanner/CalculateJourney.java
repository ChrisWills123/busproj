/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.journeyplanner;

import java.util.ArrayList;
import java.util.Date;
import r7.ibms.common.*;

/**
 *
 * Calculate journey class.
 */
public class CalculateJourney {
    
    private static String startArea;
    private static String endArea;
    private static int endBusStop;
    private static int time;
    private static Date date;
    private static int routeId = 0;
   
    
    /**
     * Constructor Method.
     * @param startStop
     * @param endStop
     * @param tripTime 
     */
    public CalculateJourney(String startarea, String endarea, int tripTime, Date tdate)
    {
        
        time = tripTime;
        date = tdate;
        startArea = startarea;
        endArea = endarea;
    }
    
    /**
     * Method to calculate the journeys needed to satisfy the parameters set by the passenger.
     * @return An arraylist of all the journeys calculated. 
     */
    public ArrayList<Journey> calculate()
    {
        // Get name of bus stop
        String[] name = startArea.split(", ");
        String startName = name[1];
        String startNameArea = name[0];
        name = endArea.split(", ");
        String endName = name[1];
        String endNameArea = name[0];
        
        ArrayList<Journey> journeys = new ArrayList<Journey>();
        
        // Load all possible points for start and end
        int[] possibleStartPoints;
        int[] possibleEndPoints;
        
        if (startName.equals("Bus Station") || startName.equals("Train Station") )
        {
          possibleStartPoints = BusStopInfo.findBusStopinArea(startNameArea, startName);
        }
        else
          possibleStartPoints = BusStopInfo.getBusStopIds(startName);
        
        if (endName.equals("Bus Station") || endName.equals("Train Station"))
            possibleEndPoints = BusStopInfo.findBusStopinArea(endNameArea, endName);
        else
            possibleEndPoints = BusStopInfo.getBusStopIds(endName);
        
        ArrayList<int[]> timingPoints = new ArrayList<int[]>();
        
        int journey_id = 0;
        
        for (int startTimingPoint : possibleStartPoints)
        {
            for (int endTimingPoint : possibleEndPoints)
            {
                if (startTimingPoint < endTimingPoint)
                {
                    if (!BusStopInfo.isTimingPoint(startTimingPoint))
                      startTimingPoint++;
                    if(!BusStopInfo.isTimingPoint(endTimingPoint))
                      endTimingPoint++;

                       int[] points = new int[2];
                       points[0] = startTimingPoint;
                       points[1] = endTimingPoint;
                       timingPoints.add(points);
                 }
                    
                }
            }        
        
        for (int[] points : timingPoints)
        {
            // Check if they have a common route
            if (Route.isInRoute(points[0]) == Route.isInRoute(points[1]))
            {
                routeId = Route.isInRoute(points[0]);
                journey_id++;
                Journey journey = new Journey(journey_id, points[0], points[1], date);
                journey.assignService(date, time, points[0], points[1], new Route(routeId));
                journeys.add(journey);
                //break;
            }
        }
        
        // If no common route was found
        if (routeId == 0)
        {
            for (int[] points : timingPoints)
            {
              if (Route.isInRoute(points[1]) == 68)
              {
                  Route startRoute = new Route(Route.isInRoute(points[0]));
                  Route endRoute = new Route(Route.isInRoute(points[1]));
                  if (startRoute.getRouteID() == 66)
                  {
                    int crossingPoint1 = startRoute.getCrossingPoint();
                    int crossingPoint2 = endRoute.getCrossingPoint();
                    
                    journey_id++;
                    Journey journey = new Journey(journey_id, points[0], points[1], date);
                    journey.assignService(date, time, points[0], crossingPoint1, startRoute);
                    journey.assignService(date, journey.getEndTime(), crossingPoint2, points[1], endRoute);
                    journey.addChangingPoint(crossingPoint1);
                    journey.addChangingPoint(crossingPoint2);
                    journeys.add(journey);
                    
                    
                  }
              }
              else if (Route.isInRoute(points[1]) == 67)
              {
                  Route startRoute = new Route(Route.isInRoute(points[0]));
                  Route endRoute = new Route(Route.isInRoute(points[1]));
                  if (endRoute.getRouteID() == 65)
                  {
                    int crossingPoint1 = startRoute.getCrossingPoint();
                    int crossingPoint2 = endRoute.getCrossingPoint();
                    
                    journey_id++;
                    Journey journey = new Journey(journey_id, points[0], points[1], date);
                    journey.assignService(date, time, points[0], crossingPoint1, startRoute);
                    journey.assignService(date, journey.getEndTime(), crossingPoint2, points[1], endRoute);
                    journey.addChangingPoint(crossingPoint1);
                    journey.addChangingPoint(crossingPoint2);
                    journeys.add(journey);
                    
                  } 
              }
            }
        }
        return journeys;
    }

}

