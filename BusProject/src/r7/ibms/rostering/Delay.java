package r7.ibms.rostering;

/**Class that generates random time delays and excuses for specific bus services
 * @author Adam Dyk
 */
public class Delay {
    private int timeDelay;
    private int excuseNo;
    private int serviceID;
    private String excuse;
    
    /**Constructor method for the Delay class
     * @param service_id The id of the service corresponding to the specific instance of the delay class
     */
    public Delay(int service_id) {
        timeDelay = -1;
        excuseNo = -1;
        serviceID = service_id;
    }
    
    /**Get method for the timeDelay
     * @return the private timeDelay integer; the time delay imposed on service
     */
    public int getTimeDelay() {
        return timeDelay;
    }
    
    /**Get method for the excuseNo
     * @return the private excuseNo integer; number of specific excuse generated
     */
    public int getExcuseNo() {
        return excuseNo;
    }
    
    /** Get method for the serviceID
     * @return the private serviceId integer; ID of the service corresponding to the class
     */
    public int getServiceID() {
        return serviceID;
    }
    
    /**Get method for the excuse
     * @return the private excuse String; String containing the excuse to be displayed
     */
    public String getExcuse() {
        return excuse;
    }
    
    /**Generates a random time delay in the range 0 to 30
     * 
     */
    public void generateTimeDelay() {
        if(Math.random() > 0.5) {
            timeDelay = (int) Math.floor(Math.random() * 21);
            this.generateExcuse();
        }
        else {
            timeDelay = 0;
        }
    }
    
    /**Generates a random excuse to be displayed to the passengers
     * 
     */
    public void generateExcuse() {
        excuseNo = (int) Math.floor(Math.random() * 11);
        selectExcuse();
    }
    
    /**Selects an excuse based on generated excuseNo
     * 
     */
    public void selectExcuse() {
        switch(excuseNo) {
                case 1:
                    excuse = "The bus has ran out of gas.";
                    break;
                case 2:
                    excuse = "The windshield wiper requires repair.";
                    break;
                case 3:
                    excuse = "Heavy traffic in the nearby area.";
                    break;
                case 4:
                    excuse = "Driver has called in sick.";
                    break;
                case 5:
                    excuse = "The bus has caught a flat tire.";
                    break;
                case 6:
                    excuse = "Bus door malfunction.";
                    break;
                case 7:
                    excuse = "Passenger refuses to pay for ticket.";
                    break;
                case 8:
                    excuse = "Air conditioning issues.";
                    break;
                case 9:
                    excuse = "Bus software requires upgrading.";
                    break;
                case 10:
                    excuse = "Bad weather conditions.";
                    break;
                default:
                    excuse = "Reason for delay unknown.";
                    break;
        }
    }
}
