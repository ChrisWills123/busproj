package r7.ibms.rostering;

import r7.ibms.common.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2sm3
 */
// Class containing methods needs for the rostering of drivers
public class DriverRoster { 
    
  // Clears the drivers hours per week in the database (pass in all drivers to
  // be cleared as argument
  public static void clearHoursPerWeek(int[]driversArray)
  {
      for (int i=0; i<driversArray.length; i++)
        DriverInfo.setHoursThisWeek(driversArray[i], 0);
  }
  
  // Find the next available driver for a given service
  public static Object[] findNextAvailableDriver(int[][] breaksAndHours, int[][] dayOff, double[][]weekHours, ArrayList driverIds, int currentRoute, Date day, int dayIndex, Service service)
  {
    int driverIndex = 0;
    SortingDrivers.sort(breaksAndHours);
    boolean foundDriver = false;
    do
    {

    // check if this driver has the current day off
    int driverIdIndex = driverIds.indexOf(breaksAndHours[driverIndex][0]);
    if (dayOff[driverIdIndex][1] == dayIndex || dayOff[driverIdIndex][2] == dayIndex)
        driverIndex++;
    // take the first available driver from the list and check if he took a break or not
    else if (breaksAndHours[driverIndex][1] == 0)
    {
        double duration = service.getDuration();
        int hoursThisWeek = DriverInfo.getHoursThisWeek(breaksAndHours[driverIndex][0]);
        // Check if this new service is during another service the driver is already driving
        boolean overriding_service = service.overridingService(breaksAndHours[driverIndex][0], day);
        if (( !overriding_service && (breaksAndHours[driverIndex][2] + duration) <= 5) && (hoursThisWeek + duration + weekHours[driverIdIndex][1] <= 50))
          // If it isnt an overriding service, we've found a suitable driver
          foundDriver = true;
        else if (overriding_service)
          // If it is overriding then check the next driver
            driverIndex++;
        else
        {
         // Otherwise this driver needs a break, give him one
          breaksAndHours[driverIndex][1] = 1;
          driverIndex++;
        }
    }
    else
    {
        double duration = service.getDuration();
        int hoursThisWeek = DriverInfo.getHoursThisWeek(breaksAndHours[driverIndex][0]);
        
        // Check if it is an overriding service
        boolean overriding_service = service.overridingService(breaksAndHours[driverIndex][0], day);

        if (overriding_service)
          driverIndex++;
        // If the driver meets the contraints, we've found a suitable driver
        else if (((breaksAndHours[driverIndex][2] + duration) <= 10) && (hoursThisWeek + duration +  weekHours[driverIdIndex][1] <= 50))
            foundDriver = true;
        else
        {
          breaksAndHours[driverIndex][1] = 1;
          driverIndex++;
        }
    }
    } while (!foundDriver);
    
    // Return an object containing the update breaks and id of the driver
    Object[] returnObject = new Object[2];
    returnObject[0] = driverIndex;
    returnObject[1] = breaksAndHours;
    
    return returnObject;
    
  }
  
  // Returns an array which can be used to keep track of breaks and hours
  // array[][0] = driver_id
  // array[][1] = if they have had a break on this day (0=no, 1=yes)
  // array[][2] = how many hours they have worked on this day
  public static int[][] initialiseBreaksAndHours(int[] availableDrivers)
  {
      int[][] breaksAndHours = new int[availableDrivers.length][3];
      for (int n = 0; n < availableDrivers.length; n++)
      {
          breaksAndHours[n][0] = availableDrivers[n]; //driver's id
          breaksAndHours[n][1] = 0; // if he took a break or not (0/1)
          breaksAndHours[n][2] = 0; // his driving hours per day
      }
      return breaksAndHours;
  }
  
  
  // Returns an array containing the days a driver has off
  // array[][0] = driver_id
  // array[][1] = first day they have off
  // array[][2] = second day they have off
  public static int[][] getDaysOff(int[] drivers, Date startingDay)
  {
    int dayOff[][] = new int[drivers.length][3];
    for (int n = 0; n < drivers.length; n++)
       {
            int dayOne = randomDay();
            int dayTwo = randomDay();
            
            // Create new date for day one
            Calendar c = Calendar.getInstance();
            c.setTime(startingDay);
            c.add(Calendar.DATE,dayOne-1);
            Date newDate = c.getTime();
            
            // Check if at least 10 drivers are available on this day
            while (Driver.getAvailableDrivers(newDate).length < 10)
            {
                dayOne = randomDay();
                // Create new date for day one
                c = Calendar.getInstance();
                c.setTime(startingDay);
                c.add(Calendar.DATE,dayOne-1);
                newDate = c.getTime();
            }
            
            // Create new date for day two
            c = Calendar.getInstance();
            c.setTime(startingDay);
            c.add(Calendar.DATE,dayTwo-1);
            newDate = c.getTime();
            // Check if at least 10 drivers are available on this day
            while (Driver.getAvailableDrivers(newDate).length < 10 || dayOne == dayTwo)
            {
                dayTwo = randomDay();
                c = Calendar.getInstance();
                c.setTime(startingDay);
                c.add(Calendar.DATE,dayTwo-1);
                newDate = c.getTime();
            }
            
            dayOff[n][0] = drivers[n];
            dayOff[n][1] = dayOne;
            dayOff[n][2] = dayTwo;
       }
    return dayOff;
  }
  
  // Updates the hours per week for all given drivers
   // array[][0] = driver ids, array[][1]=hours
  public static void updateHoursPerWeek(double[][] hoursArray)
  {
    for (int i=0; i<hoursArray.length; i++)
        DriverInfo.setHoursThisWeek((int) hoursArray[i][0], (int)hoursArray[i][1]);
  }
  
  
  
  // Generates a random day between the range 1-7
  public static int randomDay()
  {
      return (int) (1 + (Math.random() * (8 - 1)));
  }
    
}
