package r7.ibms.rostering;

import r7.ibms.common.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2sm3
 */
public class Roster {

private int rosterId;
private Date day;

  public Roster(Date theDay)
  {
    rosterId = RosterInfo.createRoster(theDay);
    day = theDay;
  }
 
  // Generates the roster, for both buses and drivers for the given week
  public void generate() throws ParseException
  {
         //  Start at current route 65
      int currentRoute = 65;
      
      
      // Create a new bus roster
      BusRoster busRoster = new BusRoster();
      int[] busList = busRoster.getBusList();
      int drivers[] = DriverInfo.getDrivers();
      // Reset the total hours for this week
      DriverRoster.clearHoursPerWeek(drivers);
      
      // Array which contains the days a driver takes off in a week
      int[][] dayOff = DriverRoster.getDaysOff(drivers, day);
      
      // Array to keep track of how many hours driver has worked during week
      double weekHours[][] = new double[drivers.length][2];
      for (int i=0; i<drivers.length;i++)
      {
        weekHours[i][0] = drivers[i];
        weekHours[i][1] = 0.0;
      }
      
      // Create an arraylist of driver ids so they can be used when arrays are
      // sorted
      ArrayList<Integer> driverIds = new ArrayList<Integer>();
      for (int driverId : drivers)
          driverIds.add(driverId);
      
      // for each day of the week
      for (int i=1; i<=7; i++)
      {
          System.out.println("day: " + i);
          // Get available buses for the current day
          double[] availableBusList = busRoster.getAvailableBusList(busList, day);
          // Sort the buses so the ones with least hours are at beginning
          double[][] sortedBusList = busRoster.sortBusList(availableBusList);
          
          // Get available drivers for the current day and create an array
          // to keep track of whether they have taken a break and their hours
          int[] availableDrivers = Driver.getAvailableDrivers(day);           
          int[][] breaksAndHours = DriverRoster.initialiseBreaksAndHours(availableDrivers);
         
          
          // for each route (65,66,67,68) of that day
          for (currentRoute = 65; currentRoute <= 68; currentRoute++)
          {
              //find all the services running on the current day
              int[] allServices = TimetableInfo.getServices(currentRoute, TimetableInfo.timetableKind(day));
              // for each service service (i.e. assign drivers for all bus stops)
              for (int service_id : allServices)
              {
                 Service service = new Service(service_id, currentRoute);
                 sortedBusList = busRoster.sortBusList(availableBusList);
                 SortingDrivers.sort(breaksAndHours);
                 
                 // Get next available driver, as well as updated breaks
                 Object[] object = DriverRoster.findNextAvailableDriver(breaksAndHours, dayOff, weekHours, driverIds, currentRoute, day, i, service);
                 int driverIndex = (int) object[0];
                 breaksAndHours = (int[][]) object[1];
                 int driverIdIndex = driverIds.indexOf(breaksAndHours[driverIndex][0]);
                 
                 // Assign the driver and bus and update in database
                 int busID = busRoster.getNextAvailableBus(sortedBusList, service, day);
                 double duration = service.getDuration();
                 breaksAndHours[driverIndex][2] += duration;
                 weekHours[driverIdIndex][1] += duration;
                 busRoster.updateBusHours(busID, duration);
                 RosterInfo.assignDriverToTimetable(rosterId, service, busID, day, breaksAndHours[driverIndex][0]);
              }
          }
          
          // Incrementing the date by one day
          Calendar c = Calendar.getInstance();
          c.setTime(day);
          c.add(Calendar.DATE,1);
          day = c.getTime();
          

      }
        DriverRoster.updateHoursPerWeek(weekHours);
        System.out.println("finished");
  }
  
  // Returns the roster id
  public int getRosterId()
  {
      return rosterId;
  }
  
  // Returns the week starting date of the roster
  public Date getDate()
  {
      return day;
  }
  
  
}
