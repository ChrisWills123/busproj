package r7.ibms.rostering;

import r7.ibms.common.*;

import java.util.Arrays;
import java.util.Comparator;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2sm3
 */



public class SortingDrivers { 
    
    // Sorts the drivers first into ascending order by hours, if two drivers are
    // equal then sort by breaks (ascending)
    public static int[][] sort(int[][] toBeSorted)
    {
      Arrays.sort(toBeSorted, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if (((Integer) o1[2]).compareTo(o2[2]) != 0)
                {
                  return ((Integer) o1[2]).compareTo(o2[2]);
                }
                else
                  return (((Integer) o1[1]).compareTo(o2[1]));
            }
        });
      
      return toBeSorted;
    }
    
    public static int[][] sortWeek(int[][] toBeSorted)
    {
      Arrays.sort(toBeSorted, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                  return (((Integer) o2[1]).compareTo(o1[1]));
            }
        });
      
      return toBeSorted;
    }
    
    /// IN THIS FILE ONLY TEMPORARILY - MOVE TO SCHEDULE LATER
    public void updateHoursPerWeek(int[][] hoursArray)
    {
      for (int i=0; i<hoursArray.length; i++)
      {
          int currentHours = DriverInfo.getHoursThisWeek(hoursArray[i][0]);
          DriverInfo.setHoursThisWeek(hoursArray[i][0], currentHours + hoursArray[i][2]);
      }
    }
    
    /// IN THIS FILE ONLY TEMPORARILY - MOVE TO SCHEDULE LATER
    public void updateHoursForYear(int[][] hoursArray)
    {
      for (int i=0; i<hoursArray.length; i++)
      {
          // Update the hours for the year, adding this weeks on
          int currentHours = DriverInfo.getHoursThisWeek(hoursArray[i][0]);
          int currentYearHours = DriverInfo.getHoursThisYear(hoursArray[i][0]);
          DriverInfo.setHoursThisYear(hoursArray[i][0], currentHours + currentYearHours);
          
          // Reset the week hours to zero
          DriverInfo.setHoursThisWeek(hoursArray[i][0], 0);
      }
    }
    
}
