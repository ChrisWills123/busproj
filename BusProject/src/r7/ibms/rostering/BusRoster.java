package r7.ibms.rostering;

import r7.ibms.common.*;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

// The class undertakes the selection of an appropriate bus
public class BusRoster {
    
    // BusRoster constructor method
    public BusRoster() {}
    
    // Retrieves the list of buses from the database
    public int[] getBusList() {
        int[] busList = BusInfo.getBuses();
        return busList;
    }
    
    // Selects only the available buses
    public double[] getAvailableBusList(int[] busList, Date date) {
        // Create new ArrayList for found available buses
        ArrayList<Integer> availableBusList = new ArrayList<>();
        // Check in the database if the bus is available
        for(int i = 0; i < busList.length; i++) {
            // If true, append to ArrayList
            if(BusInfo.isAvailable(busList[i], date))
                availableBusList.add(busList[i]);
        }
        // Copy contents of ArrayList into a new int array
        double[] availableBuses = new double[availableBusList.size()];
        for(int i = 0; i < availableBuses.length; i++) {
            availableBuses[i] = availableBusList.get(i).intValue();
        }
        return availableBuses;
    }
    
    // Retrieves current amount of hours for each bus
    public double[][] getBusHours(double[] busList) {
        double[][] busHoursList = new double[busList.length][2];
        // Looks up each bus in the database for current hours count
        for(int i = 0; i < busList.length; i++) {
            // Copy over the bus ID
            busHoursList[i][0] = busList[i];
            if (busList[i] == 0) throw new InvalidQueryException("Nonexistent bus");
            // Retrieve hours driven for the specific bus
            busHoursList[i][1] = Double.parseDouble(database.busDatabase.get_string("bus", (int) busList[i], "hours"));
        }
        // Returns list of current bus hours
        return busHoursList;
    }
    
    // Sorts the bus list according to hours
    public double[][] sortBusList(double[] busList) {
        double[][] busHoursList = new double[busList.length][2];
        // Calls getBusHours and retrieves current bus hours count
        busHoursList = this.getBusHours(busList);
        // Sorts the 2d array according to hours
        Arrays.sort(busHoursList, new Comparator<double[]>() {
            // Initaties compare method to determine the correct order
            @Override
            public int compare(double[] o1, double[] o2) {
                return (((Double) o1[1]).compareTo(o2[1]));
            }
        });
        // Returns a sorted list of buses according to hours count
        return busHoursList;
    }
    
    // Updates the number of hours driven by bus
    public void updateBusHours(int bus_id, double hours) {
        // Gets current number of hours
        double currentHours = Double.parseDouble(database.busDatabase.get_string("bus", bus_id, "hours"));
        currentHours += hours;
        // Updates database with new hour total
        database.busDatabase.set_value("bus", bus_id, "hours", ""+currentHours);
    }
  
  // Takes the bus list to be searched, the current day and the service to be
  // assigned, and returns the next most suitable bus
  public static int getNextAvailableBus(double[][]busList, Service service, Date day)
  {
     // Initial index for searching the buses
     int bus_index = 0;
 
     boolean busFound = false;
     while (!busFound)
     {
        // The id of the current bus
        int bus_id = (int) busList[bus_index][0];
        
        // Array containing all the schedules assigned to that bus for
        // current day
        int[] ids = RosterInfo.getBusSchedules(bus_id, day);
        
        // If that bus doesn't have any schedules then return it as an
        // appropriate bus
        if (ids.length == 0)
        {
            busFound = true;
            return bus_id;
        }

        // Search through each schedule and determine if it's already in use
        // during the service we want to assign to
        boolean overridingFound = false;
        for (int id : ids)
        {
            // Current schedule that we are checking
            Object schedule[] = RosterInfo.getScheduleInfo(id);
            // The service associated to that schedule
            Service oldService = (Service) schedule[1];
            
            // The starting and end times of the schedule we are checking against
            int starting = oldService.getStartTime();
            int ending = oldService.getEndTime();

            // Date objects needed to compare the time/days
            Calendar c = Calendar.getInstance();
            c.setTime(day);
            c.add(Calendar.MINUTE, service.getStartTime());
            Date startDate = c.getTime();

            Date endDate;

            // Check to see if the end time is less than the start time
            // This can happen when a service finishes on a different day
            // to when it started
            if (service.getEndTime() < service.getStartTime())
            { 
              c.setTime(day);
              c.add(Calendar.DATE,1);
              c.add(Calendar.MINUTE, service.getEndTime());
              endDate = c.getTime();
            }
            else
            {
              c.setTime(day);
              c.add(Calendar.MINUTE, service.getEndTime());
              endDate = c.getTime();
            }
            c.setTime(day);
            c.add(Calendar.MINUTE, starting);
            Date oldstartingDate = c.getTime();

            Date oldendingDate;
            
            // Check to see if the end time is less than the start time
            // This can happen when a service finishes on a different day
            // to when it started (same as above but for the current service
            // in the schedule we are checking
            if (ending < starting)
            { 
              c.setTime(day);
              c.add(Calendar.DATE,1);
              c.add(Calendar.MINUTE, ending);
              oldendingDate = c.getTime();
            }
            else
            {
              c.setTime(day);
              c.add(Calendar.MINUTE, ending);
              oldendingDate = c.getTime();
            }

            // Here, the checking takes place if the new service lies between
            // the current service we are checking
            if (startDate.after(oldendingDate) || endDate.before(oldstartingDate));
            else
                overridingFound = true;
        }
        
        // If we found and overriding service then increment the bus index
        // and check the next bus
        if (overridingFound == true)
            bus_index++;
        else
            busFound = true;
     }  
        // Return the id of the bus we found
        return (int) (busList[bus_index][0]);
  }
    
}
