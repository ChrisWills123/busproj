package r7.ibms.rostering;

import r7.ibms.common.*;

import java.util.ArrayList;
import java.util.Date;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2sm3
 */
public class RosterInfo {
    
private RosterInfo()
{
}

/**
   * Create a roster, return the id the roster is designated
   */
  public static int createRoster(Date weekStarting)
  {
    Object[][] newRoster = new Object[1][2];
    
    newRoster[0][0] = new String("week_starting");
    newRoster[0][1] = (Date) weekStarting;
    
    int id = database.busDatabase.new_record("roster", newRoster);
    return id;
  }

 // Removes all data from roster tables in databases
  public static void clearRosters()
  {
    database.busDatabase.delete_record("roster");
    database.busDatabase.delete_record("RosterSchedules");
    database.busDatabase.delete_record("schedule");
    
    int[] busList = new BusRoster().getBusList();
    
    for (int id : busList)
        database.busDatabase.set_value("bus", id, "hours", 0);
    
  }

  
  /**
   * Assign a schedule for a driver - a schedule is them either fully doing a service or partially (i.e. from one bus stop to another)
   */
  public static void assignDriverToTimetable(int roster_id, Service service, int bus_id, Date date, int driver_id)
  {
    // Add the schedule to the schedule table
    Object[][] newSchedule = new Object[7][2];
    
    newSchedule[0][0] = new String("bus_stop_start");
    newSchedule[0][1] = service.getStartTime();
    
    newSchedule[1][0] = new String("bus_stop_end");
    newSchedule[1][1] = service.getEndTime();
    
    newSchedule[2][0] = new String("driver_id");
    newSchedule[2][1] = new Integer(driver_id);
    
    newSchedule[3][0] = new String("service_id");
    newSchedule[3][1] = service.getServiceId();
    
    newSchedule[4][0] = new String("bus_id");
    newSchedule[4][1] = new Integer(bus_id);
    
    newSchedule[5][0] = new String("date");
    newSchedule[5][1] = (Date) date;
    
    newSchedule[6][0] = new String("route");
    newSchedule[6][1] = service.getRoute();
    
    int schedule_id = database.busDatabase.new_record("schedule", newSchedule);
    
    // Assign the schedule to the correct roster
    Object[][] newRosterSchedule = new Object[2][2];
    newRosterSchedule[0][0] = new String("roster_id");
    newRosterSchedule[0][1] = new Integer(roster_id);
    
    newRosterSchedule[1][0] = new String("schedule_id");
    newRosterSchedule[1][1] = new Integer(schedule_id);
    database.busDatabase.new_record("RosterSchedules", newRosterSchedule);
  }
  
  // Returns the ids of all the schedules for a driver on particular day
  public static int[] getDriverSchedules(int driver_id, Date day)
  {
    
    return database.busDatabase.select_ids("schedule_id", "schedule", "driver_id", driver_id, "date", day, "");
  }
  
  // Returns the ids of all the schedules for a driver on particular day
  public static int[] getBusSchedules(int bus_id, Date day)
  {
    
    return database.busDatabase.select_ids("schedule_id", "schedule", "bus_id", bus_id, "date", day, "");
  }
  
  // Returns the ids of all the schedules on a particular day
  public static int[] getDaySchedules(Date day)
  {
    
    return database.busDatabase.select_ids("schedule_id", "schedule", "date", day, "");
  }
  
  // Returns the ids of all the rosters
  public static int[] getAllRosters()
  {
    
    return database.busDatabase.select_ids("roster_id", "roster", "");
  }
  
  // Get all schedule ids in particular roster
  public static int[] getAllScheduleIdsInRoster(int roster_id)
  {
     String source = database.join("RosterSchedules", "roster", "roster_id");  
     return database.busDatabase.select_ids("schedule_id", source, "roster.roster_id", roster_id, "");
     
  }
  
  // Get all schedules in particular roster
  public static ArrayList<Object[]> getAllSchedulesInRoster(int roster_id)
  {
     String source = database.join("RosterSchedules", "roster", "roster_id");  
     int[] schedule_ids = getAllScheduleIdsInRoster(roster_id);
     
     ArrayList<Object[]> schedules = new ArrayList<Object[]>();
     
     for (int id: schedule_ids)
     {
         Object[] schedule = getScheduleInfo(id);
         schedules.add(schedule);
     }
     
     return schedules;
     
  }
  
  
  // Returns the ids of all the schedules for a driver on particular day
  public static Object[] getScheduleInfo(int schedule_id)
  {
    // Object to return all the schedule information
    Object[] scheduleInfo = new Object[5];
    scheduleInfo[0] =  schedule_id;
    int startTime = database.busDatabase.get_int("schedule", schedule_id, "bus_stop_start");
    int endTime = database.busDatabase.get_int("schedule", schedule_id, "bus_stop_end");
    int service_id = database.busDatabase.get_int("schedule", schedule_id, "service_id");
    int route = database.busDatabase.get_int("schedule", schedule_id, "route");
    scheduleInfo[1] = new Service(service_id, route);
    scheduleInfo[2] = database.busDatabase.get_int("schedule", schedule_id, "driver_id");
    scheduleInfo[3] = database.busDatabase.get_date("schedule", schedule_id, "date");
    scheduleInfo[4] = database.busDatabase.get_int("schedule", schedule_id, "bus_id");

    
    return scheduleInfo;
  }

  // Returns each roster with its id and starting date
  public static Object[][] getRostersAndDates()
  {
    // Object to return all the schedule information
    int[] rosters = RosterInfo.getAllRosters();
    Object[][] rostersDates = new Object[rosters.length][2];
    for (int i = 0; i < rosters.length; i++)
    {
      rostersDates[i][0] = rosters[i];
      rostersDates[i][1] = database.busDatabase.get_date("roster", rosters[i], "week_starting");
    }    

    return rostersDates;
  }
}
