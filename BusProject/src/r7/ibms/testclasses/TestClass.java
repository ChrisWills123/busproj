package r7.ibms.testclasses;


import r7.ibms.rostering.*;
import r7.ibms.common.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;




/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2tt2
 */
public class TestClass {
    public static void main(String[] args) throws ParseException{
        /*Test class used to test several core methods that are used throughout the algorithms. 
         * We used two drivers for testing Schedule and Service class related methods.
         * Several test data are created when neccessary
         * We print out the result of each testing to reflect the expected output.*/
        
        
        
        
        
        
        database.openBusDatabase();
        //test to clear hours per week
        testClearHoursPerWeek();
        
        //set route as 65
        int route = 65;
        int [] allDrivers = DriverInfo.getDrivers();
        
        //two drivers we will be using
        int LeonardoDicaprio = allDrivers[0];
        int ChuckNorris = allDrivers[4];
          
        //two dates we will be using
        Date date = new SimpleDateFormat("yyyy/MM/dd").parse("2014/03/17");
        Date date2 = new SimpleDateFormat("yyyy/MM/dd").parse("2014/03/17");
        Date date3 = new SimpleDateFormat("yyyy/MM/dd").parse("2018/03/17");
        
        
        int[] driverSchedule = RosterInfo.getDriverSchedules(LeonardoDicaprio, date);
        int[] driverSchedule2 = RosterInfo.getDriverSchedules(ChuckNorris, date2);
        
        //get the day where Leonardo has driving schedule
        while(driverSchedule.length == 0)
        {
          Calendar c = Calendar.getInstance();
          c.setTime(date);
          c.add(Calendar.DATE, 1);
          date = c.getTime();
          driverSchedule = RosterInfo.getDriverSchedules(LeonardoDicaprio, date);
        }
        
        //get the day where Chuck has driving schedule
        while(driverSchedule.length == 0)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(date2);
            c.add(Calendar.DATE, 1);
            date2 = c.getTime();
            driverSchedule2 = RosterInfo.getDriverSchedules(ChuckNorris, date2);
        }
        
        //get the schedule info of the first schedule
        Object [] scheduleInfo = RosterInfo.getScheduleInfo(driverSchedule[0]);
        Object [] scheduleInfo2 = RosterInfo.getScheduleInfo(driverSchedule2[0]);
        
        //create two service objects
        Service service1 = (Service)scheduleInfo[1];
        Service service2 = (Service)scheduleInfo2[1];
        
        //get service ID of that schedule
        int serviceID = service1.getServiceId();
        int serviceID2 = service2.getServiceId();
       
        
        //test overriding service, this should return true because we retrieve the schedule currently set to Leonardo and Chuck, and assign a new schedule with same time to them
        testOverridingService(LeonardoDicaprio, service1,date);
        testOverridingService(ChuckNorris, service2,date2);
        
        //test overriding service, this should return false because no schedules are there in date3, (year 2018)
        testOverridingService(LeonardoDicaprio, service1,date3);
        testOverridingService(ChuckNorris, service2,date3);
        
        
        //create a 2d arrays storing Leonardo and Chuck, and their respective hours this week
        //arrays format - double[driver_id][hours this week]
        double [][] hoursArray = new double[2][2];
        hoursArray[0][0] = LeonardoDicaprio;
        hoursArray[1][0] = ChuckNorris;
        hoursArray[0][1] = 5;
        hoursArray[1][1] = 20;
        
        //test updateHoursPerWeek method, by passing the array we stored just now, it should update the hours this week for its respective drivers
        testUpdateHoursPerWeek(hoursArray);
        
        //test-get all rosters id
        testGetAllRosters();
        
        //test-get driver schedule. This should return all the schedules for Chuck on the date given
        testGetDriverSchedules(ChuckNorris, date);
        //test-get driver schedule. This should return all the schedules for Leonardo on the date given
        testGetDriverSchedules(LeonardoDicaprio, date);
        
        //test- get all schedules for a particular date
        testGetDaySchedules(date);
        
        //get all rosters ID
        int [] rosters = RosterInfo.getAllRosters();
        
        //test - get all schedule id in a given roster
        testgetAllScheduleIdsInRoster(rosters[0]);
        
        //test-get Leonardo's schedule on date given
        int [] schedule = RosterInfo.getDriverSchedules(LeonardoDicaprio, date);
        //get Chuck's schedule on date given
        int [] schedule2 = RosterInfo.getDriverSchedules(ChuckNorris, date2);
        
        
        //test-retrieve Leonardo first schedule on that day
        testGetScheduleInfo(schedule[0]);
        testGetScheduleInfo(schedule2[0]);
       
        //Some data used to test nextAvailableDriver method
        int [] availableDrivers = Driver.getAvailableDrivers(date);
        testGetDaysOff(availableDrivers,date);
        int [][] breakAndHours = DriverRoster.initialiseBreaksAndHours(availableDrivers);
        int [][] daysOff = DriverRoster.getDaysOff(availableDrivers, date);
        double [][] weekHours = new double[availableDrivers.length][2];
        ArrayList driverIds = new ArrayList<>();
        int dayIndex =0;
        //Object[] nextAvailableDriver = DriverRoster.findNextAvailableDriver(breakAndHours, daysOff, weekHours, driverIds, route, date, dayIndex, service2);
        
        //create two busRoster object
        BusRoster bus1 = new BusRoster();
        BusRoster bus2 = new BusRoster();
        
        //retrieve buses
        int[] busList = bus1.getBusList();
        
        //array to store buslist in double type
        double[] busListDouble = new double[busList.length];
        double [] availableBus = bus1.getAvailableBusList(busList,date);
        double[][] buss = new double[availableBus.length][1];
        
        //some data used to test getNextAvailableBus method
        for(int i=0;i<availableBus.length;i++)
        {
          buss[i][0] = availableBus[0];
        }
        //convert int to double
        for(int i=0;i<busList.length;i++)
            busListDouble[i] = (double)busList[i];
        
        //test-get available bus list
        testGetAvailableBusList(busList, date,bus1);
        
        //test-sort bus list
        testSortBusList(bus1,date);
        //testGetNextAvailableBus(buss, service1, date);
    }
    
    public static void testClearHoursPerWeek(){
        int [] allDrivers = DriverInfo.getDrivers();
        int driverA = allDrivers[1];
        int driverB = allDrivers[2];
        DriverInfo.setHoursThisWeek(driverA, 50);
        DriverInfo.setHoursThisWeek(driverB, 10);
        System.out.println("Hours before for driverA: " + DriverInfo.getHoursThisWeek(driverA));
        System.out.println("Hours before for driverB: " + DriverInfo.getHoursThisWeek(driverB));
        DriverRoster.clearHoursPerWeek(allDrivers);
        System.out.println("Hours after for driverA: " + DriverInfo.getHoursThisWeek(driverA));
        System.out.println("Hours after for driverB: " + DriverInfo.getHoursThisWeek(driverB));
    }
    
    public static void testOverridingService(int driverID, Service service, Date date){
        boolean isOverrided = false;
        isOverrided = service.overridingService(driverID,date);
        System.out.println("isOverrided : " + isOverrided);
    }
    
    public static void testGetDaysOff(int [] drivers, Date startingDay){
        int[][] daysOff = DriverRoster.getDaysOff(drivers, startingDay);
        for(int i=0; i<daysOff.length; i++){
        System.out.println("Driver ID, " + daysOff[i][0] + " gets " + daysOff[i][1]);
        System.out.println("and " + daysOff[i][2]);
    }
    }
    
    public static void testFindNextAvailableDriver(int[][] breaksAndHours, int[][] dayOff, double[][]weekHours, ArrayList driverIds, int currentRoute, Date day, int dayIndex, Service service){
        DriverRoster.findNextAvailableDriver(breaksAndHours, dayOff, weekHours, driverIds, currentRoute, day, dayIndex, service);
    }
    
    public static void testUpdateHoursPerWeek(double [][] doubleArrays){
        
        for(int i =0; i < doubleArrays.length; i++)
        {
        int hoursThisWeek = DriverInfo.getHoursThisWeek((int)doubleArrays[i][0]);
        System.out.println(doubleArrays[i][0] + ", hours This Week initially: " + hoursThisWeek);
        }
      
        
        for(int i =0; i < doubleArrays.length; i++)
        {
        int hoursThisWeek = DriverInfo.getHoursThisWeek((int)doubleArrays[i][0]);
        System.out.println(doubleArrays[i][0] + ", hours This Week after: " + hoursThisWeek);
        }
        
    }
    
    
    public static void testGetAllRosters(){
        int [] rosters = RosterInfo.getAllRosters();
        System.out.println("Roster ID: " + Arrays.toString(rosters));
    }
    
    public static void testClearRosters(){
        RosterInfo.clearRosters();
        int [] rosters = RosterInfo.getAllRosters();
        System.out.println(Arrays.toString(rosters));
    }
    
    public static void testGetDriverSchedules(int driverID, Date date){
        int [] schedules = RosterInfo.getDriverSchedules(driverID, date);
        System.out.println("Driver schedules: " + Arrays.toString(schedules));
    }
    
    public static void testGetDaySchedules(Date date){
        int [] schedules = RosterInfo.getDaySchedules(date);
        System.out.println("Day Schedule: " + Arrays.toString(schedules));
    }
    
    public static void testgetAllScheduleIdsInRoster(int rosterID){
        int [] schedules = RosterInfo.getAllScheduleIdsInRoster(rosterID);
        System.out.println("Roster schedule: " + Arrays.toString(schedules));
    }
    
    public static void testGetScheduleInfo(int schedule_id){
        Object[] scheduleInfo = RosterInfo.getScheduleInfo(schedule_id);
        System.out.println("Schedule info start:---");
        System.out.println("scheduleID: " + (int)scheduleInfo[0]);
        System.out.println("service:" + (Service)scheduleInfo[1]);
        System.out.println("driver id: " + (int)scheduleInfo[2]);
        System.out.println("date " + (Date)scheduleInfo[3]);
        System.out.println("bus ID " + (int)scheduleInfo[4]);
        System.out.println("---end");
    }
    
    public static void testGetAvailableBusList(int [] busList, Date date, BusRoster bus){
        int []buses = bus.getBusList();
        double[] availableBuses = bus.getAvailableBusList(buses,date);
        System.out.println("Available buses " + Arrays.toString(availableBuses));
    }
    
    public static void testSortBusList(BusRoster bus,Date date){
        int []buses = bus.getBusList();
       double [] AvailableBuses =  bus.getAvailableBusList(buses,date);
       bus.updateBusHours((int)AvailableBuses[0],50.0);
       System.out.println("Available buses " + Arrays.toString(AvailableBuses));
       double[][] sortedBus = bus.sortBusList(AvailableBuses);
       System.out.print("sorted : ");
       for(int i=0;i<sortedBus.length;i++)
       {
           System.out.print(" " + sortedBus[i][0]);
       }
       
    }
    
    public static void testGetNextAvailableBus(double[][]busList, Service service, Date day){
        int nextBus = BusRoster.getNextAvailableBus(busList,service,day);
        System.out.println("next available bus: "+ nextBus);
    }
}
