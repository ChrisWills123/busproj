package r7.ibms.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import r7.ibms.common.Service;
import r7.ibms.common.TimeSelector;
import r7.ibms.common.TimetableInfo;
import r7.ibms.rostering.RosterInfo;

/**A GUI class that display the list of times arriving at that timing point
 *
 * @author Ting Soon
 */
public class TimeSelector_GUI extends javax.swing.JFrame {

    /**
     * Creates new form TimeSelector_GUI
     * @param times time list for this TP
     * @param TimingPoint the timing point ID
     * @param d date
     * @param Route route ID
     */
    public TimeSelector_GUI(int[] times,int TimingPoint,Date d,int Route) {
        //initialize the variables
        thisTPTimes = times;
        TP = TimingPoint;
        date = d;
        route = Route;
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Time selection");

        jPanel1.setLayout(new java.awt.GridLayout(0, 3));
        for(int i = 0;i<thisTPTimes.length;i++)
        {
            final int index = i;
            String hours, minutes;
            int hoursValue = (int) Math.floor(thisTPTimes[i] / 60);
            int minutesValue = thisTPTimes[i] % 60;
            if(minutesValue < 10)
            minutes = "0" + Integer.toString(minutesValue);
            else
            minutes = Integer.toString(minutesValue);
            if(hoursValue < 10)
            hours = "0" + Integer.toString(hoursValue);
            else
            hours = Integer.toString(hoursValue);

            JButton newButton = new JButton(hours + ":" + minutes);
            newButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    TimeSelector.buttonPressed(index);
                }
            });
            jPanel1.add(newButton);
            jPanel1.revalidate();
            jPanel1.repaint();
        }

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("DejaVu Serif", 1, 14)); // NOI18N
        jLabel1.setText("Please select the expected arriving time");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(70, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private static int[] thisTPTimes;
    private static Date date;
    private static int route;
    private static int TP;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
}
