package r7.ibms.common;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * Route class used to create and manipulate route objects.
 */
public class Route {
    
    private int routeID;
    private String routeName;
    
    /**
     * Constructor method
     * @param route_id
     */
    public Route(int route_id)
    {
        routeID = route_id;
        routeName = BusStopInfo.getRouteName(routeID);
    }
    
    /**
     * Method used to return the ID of the route.
     * @return routeID
     */
    public int getRouteID()
    {
        return routeID;
    }

    /**
     * Method used to return the name of the route.
     * @return routeName
     */
    public String getName()
    {
        return routeName;
    }
    
    /**
     * Returns any bus stops this route has
     * @return stops
     */
    public int[] getBusStops()
    {
        int[] stops = BusStopInfo.getBusStops(routeID);
        return stops;
    }

    /**
     * Get all routes which pass through a timing point
     * @param timingPoint
     * @return routes
     */    
    public static Route[] getAllRoutesForTimingPoint(int timingPoint)
    {
        int[] routeIds = BusStopInfo.getRoutes(timingPoint);

        // Create route objects for each id and return in array
        Route[] routes = new Route[routeIds.length];
        for (int i=0; i<routeIds.length;i++)
            routes[i] = new Route(routeIds[i]);

        return routes;
    }

    /**
     * Get all services existing for this route on particular day
     * @param kind
     * @return services
     */    
    public Service[] getAllServicesForRoute(Date date)
    {
        // Get all Ids of services for this route
        int[] serviceIds = TimetableInfo.getServices(this.getRouteID(), TimetableInfo.timetableKind(date));

        // Make service objects for all service ids and add into array
        Service[] services = new Service[serviceIds.length];
        for (int i=0; i<serviceIds.length; i++)
          services[i] = new Service(serviceIds[i], routeID);

        return services;
    }
    
    /**
     * Returns the route the bus stop lies in
     * @param bus_stop_id
     * @return 
     */
    public static int isInRoute(int bus_stop_id)
    {
        int routeid;
        routeid = Integer.parseInt(database.busDatabase.get_string("path", bus_stop_id, "route")); 
                
        return routeid;
    }
    
    /**
     * Gets the id of the Marple Navigation Hotel bus stop for this route
     * @return 
     */
    public int getCrossingPoint()
    {
        // Get timing point for the stop to switch at navigation hotel
        int[] bus_stops = getBusStops();
        int crossingPoint = 0;
        for (int index=0; index < bus_stops.length; index++)
        {
            if (BusStopInfo.getFullName(bus_stops[index]).equals("Marple, Navigation Hotel"))
            {
              crossingPoint = bus_stops[index];
              break;
            }
        }
        
        return crossingPoint;
    }
}
