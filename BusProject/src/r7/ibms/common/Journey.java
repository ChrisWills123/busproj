package r7.ibms.common;

import java.util.ArrayList;
import java.util.Date;

public class Journey
{
	private int journeyID;
	private int startTime;
	private int endTime;
	private int startTimingPoint;
	private int endTimingPoint;
	private ArrayList<Service> services;
	private ArrayList<Integer> changingPoints;
        private Date theDate;

        /**
         * Creates a new journey object
         * @param journey_id
         * @param start_timing_point the starting point of the journey
         * @param end_timing_point the point where the journey ends
         * @param date the date of the journey
         */
	public Journey(int journey_id, int start_timing_point, int end_timing_point, Date date)
	{
            journeyID = journey_id;
            startTimingPoint = start_timing_point;
            endTimingPoint = end_timing_point;
            theDate = date;

            // Initialise arrays which keep track of services used and any change overs
            services = new ArrayList<Service>();
            changingPoints = new ArrayList<Integer>();
	}

	/**
         * Searches for a suitable service for the given parameters and assigns it to the journey
         * @param date
         * @param startTime
         * @param startPoint
         * @param endPoint
         * @param route 
         */
	public void assignService(Date date, int startTime, int startPoint, int endPoint, Route route)
	{
            System.out.println("begin assign service");
            // Get all services for the route/kind
            Service[] routeServices = route.getAllServicesForRoute(date);

            for (Service service : routeServices)
                // Check if service starts and ends at the required timing points
                if (service.passesTimingPoint(startPoint, date) && service.passesTimingPoint(endPoint, date))
                {
                        // Check if the service arrives at the start timing point after or equal depart time
                        if (service.getTimeForTimingPoint(startPoint, date) >= startTime)
                        {	// We've found a suitable service
                                services.add(service);
                                endTime = service.getTimeForTimingPoint(endPoint, date);
                                System.out.println("found service");
                                break;
                        }
                }
            System.out.println("end assign service");
	}
        

        /**
	* Required when passenger has to change over at end of service and board on another service at depot
	* @return endTime of the journey
	*/
	public int getEndTime()
	{
            return endTime;
	}
        
        /**
	* No of services in this journey
	* @return size of services list
	*/
	public int getNoOfServices()
	{
            return services.size();
	}
        
        /**
	* No of services in this journey
	* @return list of services
	*/
	public ArrayList<Service> getServices()
	{
            return services;
	}
        
        /**
	* Start time of this journey
	* @return the start time
	*/
	public int getStartTime()
	{
            return startTime;
	}
        
        /**
	* Returns a list of entries for this journey which can then be used in gui
        * entry[0] = id of the journey
        * entry[1] = start time
        * entry[2] = end time
        * entry[3] = Id of the service
        * entry[4] = the starting point
        * entry[5] = ending point
	* @return array list of entries
	*/
	public ArrayList<int[]> getEntries()
	{
            ArrayList<int[]> entries = new ArrayList<int[]>();
            
            // Check if this journey has any services assigned
            if (services.size() == 0)
            {
               return null;
            }
            else 
            {
                // If no changing points, only one service for the whole journey
                if (changingPoints.size() == 0)
                {
                    int entry[] = new int[6];
                    entry[0] = journeyID;
                    entry[1] = services.get(0).getTimeForTimingPoint(startTimingPoint, theDate);
                    entry[2] = endTime;
                    entry[3] = services.get(0).getServiceId();
                    entry[4] = startTimingPoint;
                    entry[5] = endTimingPoint;
                    entries.add(entry);
                }
                else
                {
                    // Entry for the starting point up to the change point
                    int entry[] = new int[6];
                    entry[0] = journeyID;
                    entry[1] = services.get(0).getTimeForTimingPoint(startTimingPoint, theDate);
                    entry[2] = services.get(0).getTimeForTimingPoint(changingPoints.get(0), theDate);
                    entry[3] = services.get(0).getServiceId();
                    entry[4] = startTimingPoint;
                    entry[5] = changingPoints.get(0);
                    entries.add(entry);

                    // Entry for the change point up to the end point
                    int entry2[] = new int[6];
                    entry2[0] = journeyID;
                    entry2[1] = services.get(1).getTimeForTimingPoint(changingPoints.get(1), theDate);
                    entry2[2] = services.get(1).getTimeForTimingPoint(endTimingPoint, theDate);
                    entry2[3] = services.get(1).getServiceId();
                    entry2[4] = changingPoints.get(1);
                    entry2[5] = endTimingPoint;
                    entries.add(entry2);
                }

                return entries;
            }
	}
        
        /**
         * Add a changing point to the journey
         * @param point 
         */
        public void addChangingPoint(int point)
        {
            changingPoints.add(point);
        }
}