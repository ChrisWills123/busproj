package r7.ibms.common;

import java.util.Arrays;
import java.util.Date;
import r7.ibms.gui.TimeSelector_GUI;
import r7.ibms.gui.TimingPointSelector_GUI;
import static r7.ibms.gui.TimingPointSelector_GUI.TPs;
import static r7.ibms.gui.TimingPointSelector_GUI.date;
import static r7.ibms.gui.TimingPointSelector_GUI.route;
import r7.ibms.rostering.Delay;

/**A class that handles a specific timing point the passenger has clicked
 *
 * @author Ting Soon
 */
public class TimingPointSelector {
    
    //variable for storing the timing points
    public static int[] TPs;
    //date
    public static Date date;
    //route
    public static int route;
    //delays
    public static Delay[] delayArray;
    
    /**This method will be called when route and date is entered
     * 
     * @param timingPoints  the list of timing points on the route
     * @param d the date 
     * @param Route the route ID
     */
    public static void chooseTimingPoint(int[] timingPoints, Date d, int Route){
     
        TPs = timingPoints;
        date = d;
        route = Route;
       
        //get all services for that day
        int [] allServices = TimetableInfo.getServices(route, TimetableInfo.timetableKind(d));
        
        //create delay array for services
        delayArray = new Delay[allServices.length];
        
        //for every service, instantiate a delay and generate a random excuse
        for(int i = 0; i < delayArray.length; i++) {
            delayArray[i] = new Delay(allServices[i]);
            delayArray[i].generateTimeDelay();
        }
        
        //display the GUI
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TimingPointSelector_GUI(TPs,date,route,delayArray).setVisible(true);
            }
        });
        
    }
    
    /**This method will be called when a TP is chosen
     * 
     * @param TPindex the index of the timing point in the list of all timing points
     */
    public static void buttonPressed(int TPindex){
        //get all services
        int[] services = TimetableInfo.getServices(route,TimetableInfo.timetableKind(date));
        //create Service array
        Service[] servs = new Service[services.length];
        //count for number of services
        int numServices = 0;
        
        //for every service, instantiate a Service object for it
        //and check if this service passes through this timing point
        for(int i =0; i<services.length; i++){
            servs[i] = new Service(services[i],route);
            if(servs[i].passesTimingPoint(TPs[TPindex], date))
                numServices++;
        }
        
        //create an array for the list of times arriving at this timing point
        int[] thisTPTimes = new int[numServices];
        //index for this array
        int j=0;
        
        //for every Service, check if that service passes through this TP
        //if yes, then get its time and store it to thisTPTimes array
        for(int i=0;i<servs.length;i++){
            if(servs[i].passesTimingPoint(TPs[TPindex],date)){
              thisTPTimes[j] = servs[i].getTimeForTimingPoint(TPs[TPindex], date);
              j++;
            }
        }
        //pass the time list, timing point, date,route, and delay to TimeSelector
        TimeSelector.chooseTime(thisTPTimes,TPs[TPindex],date,route,delayArray);
    }
    
}
