package r7.ibms.common;

import r7.ibms.rostering.SortingDrivers;
import java.util.ArrayList;
import java.util.Date;

public abstract class Driver{
    private int driver_id;
    public abstract boolean makeHolidayRequest(Date startDate, Date endDate, int driver);
    public static int[] getAvailableDrivers(Date day)
    {
        // Get list of all drivers
        int[] driver_ids = DriverInfo.getDrivers();
        
        // Find drivers available on the given date
        ArrayList<Integer> available = new ArrayList<Integer>();
        for (int driver_id : driver_ids)
            if (DriverInfo.isAvailable(driver_id, day))
                available.add(driver_id);
        
        // Convert the array list to required integer array type
        int[][] available_drivers = new int[available.size()][2];
        for (int i=0; i<available.size(); i++)
        {
            available_drivers[i][0] = available.get(i);
            available_drivers[i][1] = DriverInfo.getHoursThisWeek(available.get(i));
        }
        available_drivers = SortingDrivers.sortWeek(available_drivers);
        
        int[] sortedDrivers = new int[available.size()];
        for (int i=0; i<available.size(); i++)
        {
            sortedDrivers[i] = available_drivers[i][0];
        }

        return sortedDrivers;
    }
}
