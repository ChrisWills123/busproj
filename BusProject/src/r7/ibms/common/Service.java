package r7.ibms.common;


import java.util.Arrays;
import r7.ibms.rostering.RosterInfo;
import java.util.Calendar;
import java.util.Date;

public class Service {
    
    private int serviceId;
    private int startTime;
    private int endTime;
    private int route;
    
    // Constructor for service when route is known
    public Service(int service_id, int currentRoute)
    {
        serviceId = service_id;
        
        // Retrieve the start and end times for the service
        int[] serviceTimes = TimetableInfo.getServiceTimes(currentRoute, service_id);
        startTime = serviceTimes[0];
        endTime = serviceTimes[serviceTimes.length - 1];
        route = currentRoute;
    }
    
    
    // Returns the route of this service
    public int getRoute()
            
    {
        return route;
    }
    
    // Returns the start time of this service
    public int getStartTime()
    {
        return startTime;
    }
    
    // Returns the end time of this service
    public int getEndTime()
    {
        return endTime;
    }
    
    // Returns the service id of this service
    public int getServiceId()
    {
        return serviceId;
    }
    
    // Returns the duration of this service
    public double getDuration()
    {
      int startingPoint = startTime;
      int endingPoint = endTime;
      
      // If ending point is on next day, add 24 hours to it
      if (startingPoint > endingPoint)
          endingPoint += 1440;
      
      return (double)(endingPoint - startingPoint)/60.0;
    }
    
  // Checks if this service lies in between any that a driver has on a particular
  // day
  public boolean overridingService(int driver_id, Date day)
  {
     int[] ids = RosterInfo.getDriverSchedules(driver_id, day);
     Service service = this;
      
     for (int id : ids)
     {
        // Current schedule that we are checking
        Object schedule[] = RosterInfo.getScheduleInfo(id);
        // The service associated to that schedule
        Service oldService = (Service) schedule[1];

        // The starting and end times of the schedule we are checking against
        int starting = oldService.getStartTime();
        int ending = oldService.getEndTime();

        // Date objects needed to compare the time/days
        Calendar c = Calendar.getInstance();
        c.setTime(day);
        c.add(Calendar.MINUTE, service.getStartTime());
        Date startDate = c.getTime();

        Date endDate;

        // Check to see if the end time is less than the start time
        // This can happen when a service finishes on a different day
        // to when it started
        if (service.getEndTime() < service.getStartTime())
        { 
          c.setTime(day);
          c.add(Calendar.DATE,1);
          c.add(Calendar.MINUTE, service.getEndTime());
          endDate = c.getTime();
        }
        else
        {
          c.setTime(day);
          c.add(Calendar.MINUTE, service.getEndTime());
          endDate = c.getTime();
        }
        c.setTime(day);
        c.add(Calendar.MINUTE, starting);
        Date oldstartingDate = c.getTime();

        Date oldendingDate;

        // Check to see if the end time is less than the start time
        // This can happen when a service finishes on a different day
        // to when it started (same as above but for the current service
        // in the schedule we are checking
        if (ending < starting)
        { 
          c.setTime(day);
          c.add(Calendar.DATE,1);
          c.add(Calendar.MINUTE, ending);
          oldendingDate = c.getTime();
        }
        else
        {
          c.setTime(day);
          c.add(Calendar.MINUTE, ending);
          oldendingDate = c.getTime();
        }

        // Here, the checking takes place if the new service lies between
        // the current service we are checking
        if (startDate.after(oldendingDate) || endDate.before(oldstartingDate));
        else
           return true;
     }
     // If we get here, no overriding service has been found so return false
     return false;
  }

   /**
    * Checks if the service stops at a particular timing point
    * @return true or false
    **/
    public boolean passesTimingPoint(int timingPoint, Date date)
    {
        // Get all timing points for this service
        int[] timingPoints = TimetableInfo.getServiceTimingPoints(route, TimetableInfo.timetableKind(date), serviceId);
              
        // Check if the given timing point is within the array
        for (int i=0; i<timingPoints.length; i++)
          if (timingPoints[i] == timingPoint)
            return true;

        // If we get here, means the timing point does not lie within this service
        return false;
    }

   /**
    * Gets the time the service arrives a particular timing point
    * @return serviceTimes[i], -1 if service does not go through that timing point
    **/
    public int getTimeForTimingPoint(int timingPoint, Date date)
    {
        // Get all timing points and times for this service
        int[] timingPoints = TimetableInfo.getServiceTimingPoints(route, TimetableInfo.timetableKind(date), serviceId);
        int[] serviceTimes = TimetableInfo.getServiceTimes(route, date, serviceId);

        // Check if the given timing point is within the array and return it's time
        for (int i=0; i<timingPoints.length; i++)
        {
          if (timingPoints[i] == timingPoint)
          {
            return serviceTimes[i];
          }
        }

        // If we get here, the timing point given doesn't lie on the service
        return -1;
    }

   /**
    * Gets the ids of all the timing points for this service
    * @return timingPoints
    **/
    public int[] getTimingPoints(Date date)
    {
        // Get all timing points and times for this service
        int[] timingPoints = TimetableInfo.getServiceTimingPoints(route, TimetableInfo.timetableKind(date), serviceId);

        return timingPoints;
    }
    
}
