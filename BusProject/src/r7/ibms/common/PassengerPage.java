package r7.ibms.common;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import r7.ibms.rostering.RosterInfo;
import r7.ibms.gui.PassengerPageGUI;
import static r7.ibms.gui.PassengerPageGUI.dateCorrect;

/**A class that perform some operation for the GUI
 *
 * @author Ting Soon
 */
public class PassengerPage {
    
    /**method to get list of timing points in the route
     * 
     * @param routeName route name entered by passenger
     * @return 
     */
    public static int[] getTimingPoints(String routeName){
        int route = TimetableInfo.findRoute(routeName);
        
        int[] timingPoints = TimetableInfo.getTimingPoints(route);
        return timingPoints;
    }
    
    /**method to check whether a route name is a valid route
     * 
     * @param routeName the route number entered by passenger
     */
    public static boolean isValidRouteName(String routeName){
        int [] routes = BusStopInfo.getRoutes();
        boolean isValid = false;
        for(int i =0; i<routes.length && !isValid;i++)
        {
          if(BusStopInfo.getRouteName(routes[i]).equals(routeName))
            isValid = true;
        }
        return isValid;
    }
    
}
