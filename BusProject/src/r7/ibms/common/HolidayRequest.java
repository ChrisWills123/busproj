package r7.ibms.common;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2ad2
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HolidayRequest {
    
    private Date dateFrom;
    private Date dateTo;
    private int driver_id;
    private int request_id;
    private static int highest_id;
    private boolean approved = false;
    private static String filename = "/home/mbax2ad2/COMP23420/busProject/busproject/BusProject/src/r7/ibms/common/requests.txt";
    //private static String filename = "/Users/shakil/uniwork/COMP23420/busproject/BusProject/src/requests.txt";
    
    // Constructor for totally new request (it needs an id)
    public HolidayRequest(Date startDate, Date endDate, int driver) {
        dateFrom = startDate;
        dateTo = endDate;
        driver_id = driver;
        request_id = getLatestId();
       
    }
    
    // Constructor for when id is known
    public HolidayRequest(int requestid, Date startDate, Date endDate, int driver) {
        dateFrom = startDate;
        dateTo = endDate;
        driver_id = driver;
        request_id = requestid;
    }
    
    public Date getDateFrom() {
        return dateFrom;
    }
    
    public Date getDateTo() {
        return dateTo;
    }
    
    public int getDriverID() {
        return driver_id;
    }
    
    public int getRequestID() {
        return request_id;
    }
    
    public boolean verifyRequest() {
        // Checks correct date order/format
        if(dateTo.before(dateFrom))
            return false;
        // Find number of days to take off
        int dateDiff = (int)((dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24));
        int holidayCount = DriverInfo.getHolidaysTaken(driver_id);
        // Check if request doesn't exceed holiday limit
        if((dateDiff + holidayCount) > 25)
            return false;
        // Get all driverIDs
        int[] driverIDs = DriverInfo.getDrivers();
        boolean accept = true;
        // Check for each date if there is at least 11 drivers available
        Calendar c = Calendar.getInstance();
        c.setTime(dateFrom);
        //for(Date i = dateFrom; c.before(dateTo); c.add(Calendar.DATE,1))
        Date i = c.getTime();
        while(i.before(dateTo)) {
            int driverCount = 0;
            System.out.println("for1");
            // Checks availability of each driver on a specific day
            for(int j = 0; j < driverIDs.length; j++) {
                System.out.println("for2");
                if(DriverInfo.isAvailable(driverIDs[j],i))
                    driverCount++; // Increments # of drivers available
            }
            // Rejects request if one requested date doesn't have enough drivers
            if(driverCount < 11) {
                accept = false;
                break;
            }
            System.out.println("end");
            c.add(Calendar.DATE,1);
            i = c.getTime();
        }
        // Write to File with Pending Status
        requestsWrite();
        return accept;
    }
    
    // Returns the newest id that can be used for next holiday request
    private int getLatestId()
    {
        Path currentRelativePath = Paths.get("");
        filename = currentRelativePath.toAbsolutePath().toString() + "/src/r7/ibms/common/requests.txt";
        try
        {
          BufferedReader reader = new BufferedReader(new FileReader(filename));
          int id = Integer.parseInt(reader.readLine());
          return id+1;
          
        } catch (Exception e)
        {
            System.out.println(e);
            return 0;
        }
    }
    
    // Returns all existing holiday requests (as stored in file)
    public static ArrayList<HolidayRequest> getAllHolidayRequests()
    {
        ArrayList<HolidayRequest> requests = new ArrayList<HolidayRequest>();
        Path currentRelativePath = Paths.get("");
        filename = currentRelativePath.toAbsolutePath().toString() + "/src/r7/ibms/common/requests.txt";
        try
        {
          BufferedReader reader = new BufferedReader(new FileReader(filename));
          String currentLine;
          int counter = 0;
          // Reads first line containing highest id
          highest_id = Integer.parseInt(reader.readLine());
          // Read each line in file
          while ((currentLine = reader.readLine()) != null) 
          {
            // Split line according to '-' delimeter, assign variables accordingly
	    String[] parts = currentLine.split("-");
            int id = Integer.parseInt(parts[0]);
            int driverid = Integer.parseInt(parts[1]);
            Date datefrom = new SimpleDateFormat("yyyy/MM/dd").parse(parts[2]);
            Date dateto = new SimpleDateFormat("yyyy/MM/dd").parse(parts[3]);
            boolean apprvd = parts[4].equals("true");
            // Store the holiday request in array
            HolidayRequest request = new HolidayRequest(id, datefrom, dateto, driverid);
            // Set the correct approved value
            if (apprvd)
                request.approved = true;
            requests.add(request);
	  }
          reader.close();
          return requests;
          
        } catch (Exception e)
        {
            System.out.println(e);
            return null;
        }
    }
    public void setApproved(boolean status)
    {
        approved = status;
        updateMe();
    }
    
    public boolean isApproved()
    {
        return approved;
    }
            
    private void requestsWrite()
    {
      Path currentRelativePath = Paths.get("");
      filename = currentRelativePath.toAbsolutePath().toString() + "/src/r7/ibms/common/requests.txt";
      ArrayList<HolidayRequest> requests = getAllHolidayRequests();
      requests.add(this);
      highest_id++;
      try 
      {
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, false)));
        out.println(highest_id);
        for (HolidayRequest request : requests)
          out.println(request.request_id + "-" + request.driver_id + "-" + (new SimpleDateFormat("yyyy/MM/dd").format(request.dateFrom)) + "-" + (new SimpleDateFormat("yyyy/MM/dd").format(request.dateTo)) + "-" + String.valueOf(request.approved));
        out.close();
      } catch (IOException e) 
      {
          //
      }
    }
    
    private void requestsUpdate(ArrayList<HolidayRequest> requests)
    {
      Path currentRelativePath = Paths.get("");
      filename = currentRelativePath.toAbsolutePath().toString() + "/src/r7/ibms/common/requests.txt";
      //ArrayList<HolidayRequest> requests = getAllHolidayRequests();
      try 
      {
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, false)));
        out.println(highest_id);
        for (HolidayRequest request : requests)
          out.println(request.request_id + "-" + request.driver_id + "-" + (new SimpleDateFormat("yyyy/MM/dd").format(request.dateFrom)) + "-" + (new SimpleDateFormat("yyyy/MM/dd").format(request.dateTo)) + "-" + String.valueOf(request.approved));
        out.close();
      } catch (IOException e) 
      {
          //
      }
    }

    // Updates the approval status in the file
    private void updateMe()
    {
      ArrayList<HolidayRequest> requests = getAllHolidayRequests();
      
      for (HolidayRequest request : requests)
      {
          if (request.getRequestID() == request_id)
             request.approved = approved;
      }
      requestsUpdate(requests);
    }
}
