package r7.ibms.common;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2ad2
 */
import java.util.Date;

public class NormalDriver extends Driver {
    
    private int driver_id;
    
    public NormalDriver(int driverID) {
        driver_id = driverID;
    }
    
    public int getDriverID() {
        return driver_id;
    }
    
    @Override
    public boolean makeHolidayRequest(Date startDate, Date endDate, int driver) {
        // Creates a new holiday request and proceeds with the verification
        HolidayRequest request = new HolidayRequest(startDate, endDate, driver);
        return request.verifyRequest();
    } 
}
