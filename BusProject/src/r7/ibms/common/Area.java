/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.common;

/**
 *
 * Area class used to create and manipulate area objects.
 */
public class Area {
    
    private static int areaId;
    private static String areaName;
    
    /**
     * Constructor method
     * @param area_id
     * @param area_name 
     */
    public Area(int area_id, String area_name)
    {
        areaId = area_id;
        areaName = area_name;
    }
    
    /**
     * Method used to return the area ID of an area object.
     * @return areaId
     */
    public static int getAreaID()
    {
        return areaId;
    }
    
    /**
     * Method used to return the id of an area when the name is known.
     * @param area_name
     * @return area ID
     */
    public static int getAreaID(String area_name)
    {
        return BusStopInfo.getAreaID(area_name);
    }
    
    /**
     * Method used to return the name of an area object.
     * @return area name
     */
    public static String getAreaName()
    {
        return areaName;
    }
    
    
    /**
     * Method to get all the timing points of an area.
     * @param area_id
     * @return area bus stops
     */
    public static int[] getAreaTimingPoints(int area_id)
    {
        return BusStopInfo.getAreaTimingPoints(area_id);
    }
    
    
   /**
    * Method to get the first Timing point of an area.
    * @param area_id
    * @return first bus stop of that area
    */
   public static int getAreaTimingPoint(int area_id)
   {
       int array[];
       array = Area.getAreaTimingPoints(area_id);
       
       return array[0];
   }
    
}
