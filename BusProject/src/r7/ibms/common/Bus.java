package r7.ibms.common;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mbax2ad2
 */
// Class defining a bus object
public class Bus {
    
    // Unique bus private id
    private int busID;
    private int currentDriver;
    private int currentService;
    
    // Construct method for bus
    public Bus(int bus_id) {
        busID = bus_id;
    }
    
    // Get method for the private busID variable
    public int getBusID() {
        return busID;
    }
    
    // Get method for the private currentDriver variable
    public int getCurrentDriver() {
        return currentDriver;
    }
    
    // Get method for the private currentService variable
    public int getCurrentService() {
        return currentService;
    }
    
    // Set method to update the current driver using the bus
    public void setCurrentDriver(int driverID) {
        currentDriver = driverID;
    }
    
    // Set method to update the current service under which the bus is operating
    public void setCurrentService(int serviceID) {
        currentService = serviceID;
    }
    
}
