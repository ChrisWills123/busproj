package r7.ibms.common;

import java.util.Arrays;
import java.util.Date;
import r7.ibms.gui.BusStatus_GUI;
import r7.ibms.gui.TimeSelector_GUI;
import r7.ibms.rostering.Delay;
import r7.ibms.rostering.RosterInfo;

/** Class that handles a user request of a specific timing point
 *
 * @author mbax2tt2
 */
public class TimeSelector {
    
    public static int[] thisTPTimes;
    public static int TP;
    public static Date date;
    public static int route;
    public static Delay[] Delays;
    
    /** Method to initialise GUI for timing point selection
     * 
     * @param times Array of arrival times on the selected bus stop
     * @param TimingPoint Current selected bus stop
     * @param d Current date
     * @param Route Current specified route
     * @param delays Array of delays corresponding to specific services
     */
    public static void chooseTime(int[] times,int TimingPoint,Date d,int Route,Delay[] delays){
        thisTPTimes = times;
        TP = TimingPoint;
        date = d;
        route = Route;
        Delays = delays;
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TimeSelector_GUI(thisTPTimes,TP,date,route).setVisible(true);
            }
        });
    }
    
    /**Method that find the Delay class corresponding to a specific timing point
     * 
     * @param timeIndex The timing point selected by the user of specific route and bus stop
     */
    public static void buttonPressed(int timeIndex){
        TimetableInfo.timetableKind kind = TimetableInfo.timetableKind(date);
        //contains all the schedules on that day
   
        int [] services = TimetableInfo.getServices(route, TimetableInfo.timetableKind(date));
        Service[] servs = new Service[services.length];
        
        int theService = 0;
        boolean foundService = false;
        
        
        for(int i=0;i<services.length && !foundService;i++){
            servs[i] = new Service(services[i],route);
            if(servs[i].passesTimingPoint(TP, date))
                if(servs[i].getTimeForTimingPoint(TP, date) == thisTPTimes[timeIndex]){
                    foundService = true;
                    theService = servs[i].getServiceId();
                }
        }
        
        Delay theDelay;
        for(int i =0; i<Delays.length;i++){
            if(Delays[i].getServiceID() == theService){
                theDelay = Delays[i];
                BusStatus_GUI.displayStatus(theDelay.getExcuse(), theDelay.getTimeDelay(), thisTPTimes[timeIndex]);
                System.out.println(thisTPTimes[timeIndex]);
                if(theDelay.getExcuse() == null)
                    System.out.println("OK");
                else{
                    
                    System.out.println(theDelay.getExcuse());
                }
                break;
            }
        }
        
    }
    
}
