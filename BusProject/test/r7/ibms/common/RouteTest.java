/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.common;

import java.text.ParseException;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;

/**
 *
 * @author mbax9kc3
 */
public class RouteTest {
    
    public RouteTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
      database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRouteID method, of class Route.
     */
    @Test
    public void testGetRouteID() {
        System.out.println("getRouteID");
        Route instance = new Route(65);
        int expResult = 65;
        int result = instance.getRouteID();
        assertEquals(expResult, result);
        
        System.out.println("getRouteID2");
        Route instance2 = new Route(689);
        int expResult2 = 689;
        int result2 = instance2.getRouteID();
        assertEquals(expResult2, result2);
        // TODO review the generated test code and remove the default call to fail.
    }
    

    /**
     * Test of getName method, of class Route.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Route instance = new Route(67);
        String expResult = "358out";
        String result = instance.getName();
        assertEquals(expResult, result);
        
        System.out.println("getName2");
        Route instance2 = new Route(678);
        String expResult2 = "";
        String result2 = instance2.getName();
        assertEquals(expResult2, result2);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getBusStops method, of class Route.
     */
    @Test
    public void testGetBusStops() {
        System.out.println("getBusStops");
        Route instance = new Route(65);
        int[] expResult = {770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780};
        int[] result = instance.getBusStops();
        assertArrayEquals(expResult, result);
        
        System.out.println("getBusStops2");
        Route instance2 = new Route(657);
        int[] result2 = instance2.getBusStops();
        assertEquals(0, result2.length);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getAllServicesForRoute method, of class Route.
     */
    @Test
    public void testGetAllServicesForRoute() throws ParseException {
        System.out.println("getAllServicesForRoute");
        Date date = new SimpleDateFormat("yyyy/MM/dd").parse("01/05/2014");
        Route instance = new Route(65);
        Service[] expResult = {new Service(6177, 65), new Service(6178, 65), new Service(6179, 65), new Service(6180, 65), 
            new Service(6181, 65), new Service(6182, 65), new Service(6183, 65), new Service(6184, 65), new Service(6185, 65), 
            new Service(6186, 65), new Service(6187, 65), new Service(6188, 65), new Service(6189, 65), 
            new Service(6190, 65), new Service(6191, 65), new Service(6192, 65), new Service(6193, 65), new Service(6194, 65),
            new Service(6195, 65), new Service(6196, 65), new Service(6197, 65), new Service(6198, 65), new Service(6199, 65), 
            new Service(6200, 65), new Service(6201, 65), new Service(6202, 65), new Service(6203, 65), new Service(6204, 65),
            new Service(6205, 65), new Service(6206, 65), new Service(6207, 65), new Service(6208, 65), new Service(6209, 65), 
            new Service(6210, 65), new Service(6211, 65), new Service(6212, 65), new Service(6213, 65), new Service(6214, 65), 
            new Service(6215, 65), new Service(6216, 65), new Service(6217, 65), new Service(6218, 65), new Service(6219, 65),
            new Service(6220, 65), new Service(6221, 65), new Service(6222, 65), new Service(6223, 65), new Service(6224, 65),
            new Service(6225, 65), new Service(6226, 65), new Service(6227, 65), new Service(6228, 65), new Service(6229, 65), 
            new Service(6230, 65), new Service(6231, 65), new Service(6232, 65), new Service(6233, 65), new Service(6234, 65), 
            new Service(6235, 65), new Service(6236, 65), new Service(6237, 65)};
        Service[] result = instance.getAllServicesForRoute(date);
        boolean passed = true;
        for (int i=0; i<expResult.length; i++)
        {
            if(result[i].getServiceId() != expResult[i].getServiceId())
                passed = false;
        }
        if (!passed)
         fail("failed");   
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isInRoute method, of class Route.
     */
    @Test
    public void testIsInRoute() {
        System.out.println("isInRoute");
        int bus_stop_id = 770;
        int expResult = 65;
        int result = Route.isInRoute(bus_stop_id);
        assertEquals(expResult, result);
        
        System.out.println("isInRoute");
        int bus_stop_id2 = 782;
        int expResult2 = 66;
        int result2 = Route.isInRoute(bus_stop_id2);
        assertEquals(expResult2, result2);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getCrossingPoint method, of class Route.
     */
    @Test
    public void testGetCrossingPoint() {
        System.out.println("getCrossingPoint");
        Route instance = new Route(65);
        int expResult = 773;
        int result = instance.getCrossingPoint();
        assertEquals(expResult, result);
        
        System.out.println("getCrossingPoint");
        Route instance2 = new Route(68);
        int expResult2 = 806;
        int result2 = instance2.getCrossingPoint();
        assertEquals(expResult2, result2);
        // TODO review the generated test code and remove the default call to fail.
       
    }
    
}