/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.common;

import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import r7.ibms.journeyplanner.CalculateJourney;

/**
 *
 * @author mbax9kc3
 */
public class JourneyTest {
    
    public JourneyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
       database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of assignService method, of class Journey.
     */
    @Test
    public void testAssignService() {
        System.out.println("assignService");

        Date date = new Date();
        
        // Find service for a simple journey with no change points
        Journey instance = new Journey(1, 770, 775, date);
        instance.assignService(date, 315, 770, 775, new Route(65));
        assertEquals(instance.getNoOfServices(),1);
        assertTrue(instance.getServices().get(0).getStartTime() > instance.getStartTime());
        assertEquals(instance.getEndTime(), instance.getServices().get(0).getTimeForTimingPoint(775, date));
        
        
        instance = new Journey(1, 771, 775, date);
        instance.assignService(date, 315, 771, 775, new Route(65));
        // Should be no services found as there is no service at timing point 771
        assertEquals(instance.getNoOfServices(), 0);
    }


    /**
     * Test of getEntries method, of class Journey.
     */
    @Test
    public void testGetEntries() {
        System.out.println("getEntries");
        Date date = new Date();
        
        // Find service for a simple journey with no change points
        Journey instance = new Journey(1, 770, 775, date);
        instance.assignService(date, 315, 770, 775, new Route(65));
        ArrayList<int[]> result = instance.getEntries();
        // Check that a result is returned
        assertNotNull(result);
        // There should be only one entry
        assertEquals(result.size(), 1);
                
    }
}