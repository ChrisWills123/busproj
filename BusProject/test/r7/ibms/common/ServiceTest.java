/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mbax2sm3
 */
public class ServiceTest {
    
    public ServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of passesTimingPoint method, of class Service.
     */
    @Test
    public void testPassesTimingPoint() throws ParseException {
        System.out.println("passesTimingPoint");
        String oldstring = "2014-05-06 00:00:00.0";
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(oldstring);
        Service instance = new Service(6177, 65);
        assertTrue(instance.passesTimingPoint(770, date));
        assertFalse(instance.passesTimingPoint(771, date));
        assertFalse(instance.passesTimingPoint(804, date));
        assertFalse(instance.passesTimingPoint(123, date));
        
    }

    /**
     * Test of getTimeForTimingPoint method, of class Service.
     */
    @Test
    public void testGetTimeForTimingPoint() throws ParseException {
        System.out.println("getTimeForTimingPoint");
        String oldstring = "2014-05-06 00:00:00.0";
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(oldstring);
        Service instance = new Service(6177, 65);
        assertEquals(instance.getTimeForTimingPoint(770, date), 340);
        assertEquals(instance.getTimeForTimingPoint(771, date), -1);
        assertEquals(instance.getTimeForTimingPoint(804, date), -1);
        assertEquals(instance.getTimeForTimingPoint(123, date), -1);
    }
}