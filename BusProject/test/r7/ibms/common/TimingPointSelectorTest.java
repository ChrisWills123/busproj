package r7.ibms.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ting Soon
 */
public class TimingPointSelectorTest {
    
    public TimingPointSelectorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of chooseTimingPoint method, of class TimingPointSelector.
     */
    @Test
    public void testChooseTimingPoint() throws ParseException {
        System.out.println("chooseTimingPoint");
        int[] timingPoints = {770, 772, 773, 775, 776, 777, 779, 780};
        Date d = new SimpleDateFormat("yyyy/MM/dd").parse("1992/1/2");
        int Route = 383;
        TimingPointSelector.chooseTimingPoint(timingPoints, d, Route);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buttonPressed method, of class TimingPointSelector.
     */
    @Test
    public void testButtonPressed() {
        System.out.println("buttonPressed");
        int TPindex = 2;
        TimingPointSelector.buttonPressed(TPindex);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}