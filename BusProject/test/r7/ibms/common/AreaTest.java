/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.common;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mbax9kc3
 */
public class AreaTest {
    
    public AreaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
         database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAreaID method, of class Area.
     */
    @Test
    public void testGetAreaID_0args() {
        System.out.println("getAreaID");
        Area testA = new Area(210, "Roomiley");
        int expResult = 210;
        int result = testA.getAreaID();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getAreaID method, of class Area.
     */
    @Test
    public void testGetAreaID_String() {
        System.out.println("getAreaID case1");
        String area_name = "Stockport";
        int expResult = 209;
        int result = Area.getAreaID(area_name);
        assertEquals(expResult, result);
        
        System.out.println("getAreaID case2");
        String area_name2 = "Marple";
        int expResult2 = 212;
        int result2 = Area.getAreaID(area_name2);
        assertEquals(expResult2, result2);
        
        
        System.out.println("getAreaID case3");
        String area_name3 = "Mexico";
        int expResult3 = 0;
        int result3 = Area.getAreaID(area_name3);
        assertEquals(expResult3, result3);
        // TODO review the generated test code and remove the default call to fail.
               
    }

    /**
     * Test of getAreaName method, of class Area.
     */
    @Test
    public void testGetAreaName_0args() {
        System.out.println("getAreaName");
        Area testArea = new Area(209, "Stockport");
        String expResult = "Stockport";
        String result = testArea.getAreaName();
        assertEquals(expResult, result);

    }

}