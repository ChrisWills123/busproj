package r7.ibms.common;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ting Soon
 */
public class PassengerPageTest {
    
    public PassengerPageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getTimingPoints method, of class PassengerPage.
     */
    @Test
    public void testGetTimingPoints() {
        System.out.println("getTimingPoints");
        String routeName = "383";
        int[] expResult = {770, 772, 773, 775, 776, 777, 779, 780};
        int[] result = PassengerPage.getTimingPoints(routeName);
        assertArrayEquals(expResult, result);
        String routeName2 ="358out";
        int [] expResult2 = {793, 794, 796, 797, 798, 799, 800, 801, 802, 803, 790, 791, 792};
        int[] result2 = PassengerPage.getTimingPoints(routeName2);
        assertArrayEquals(expResult2,result2);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of isValidRouteName method, of class PassengerPage.
     */
    @Test
    public void testIsValidRouteName() {
        System.out.println("isValidRouteName");
        String routeName = "34";
        boolean expResult = false;
        boolean result = PassengerPage.isValidRouteName(routeName);
        assertEquals(expResult, result);
        String routeName2 = "383";
        boolean expResult2 = true;
        boolean result2 = PassengerPage.isValidRouteName(routeName2);
        assertEquals(expResult2,result2);
        String routeName3 = "358out";
        boolean expResult3 = true;
        boolean result3 = PassengerPage.isValidRouteName(routeName3);
        assertEquals(expResult3,result3);
        String routeName4 = "358back";
        boolean expResult4 = true;
        boolean result4 = PassengerPage.isValidRouteName(routeName4);
        assertEquals(expResult4,result4);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}