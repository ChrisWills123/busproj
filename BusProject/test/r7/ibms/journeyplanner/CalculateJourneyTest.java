/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package r7.ibms.journeyplanner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import r7.ibms.common.Journey;
import r7.ibms.common.database;

/**
 *
 * @author mbax9kc3
 */
public class CalculateJourneyTest {
    
    public CalculateJourneyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
      database.openBusDatabase();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateTwo method, of class CalculateJourney.
     */
    @Test
    public void testCalculate() throws ParseException {
        System.out.println("calculate");
        Date date = new SimpleDateFormat("yyyy/MM/dd").parse("01/05/2014");
        CalculateJourney testjourney = new CalculateJourney("Stockport, Bus Station", "Glossop, Grouse Inn", 500, date);
        int expResult = 6529;
        ArrayList<Journey> result = testjourney.calculate();
        
        ArrayList<int[]> journeyEntries;
        int testServ = 0;
        for (Journey journey : result)
        {
          journeyEntries = journey.getEntries();
          
          for (int[] entry : journeyEntries)
          {
             testServ = entry[3];
          }
        }
        
        assertEquals(expResult, testServ);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testCalculate2() throws ParseException {
        System.out.println("calculate");
        Date date = new SimpleDateFormat("yyyy/MM/dd").parse("01/05/2014");
        CalculateJourney testjourney = new CalculateJourney("Romiley, Corcoran Drive", "Hayfield, Bus Station", 540, date);
        int expResult = 6332;
        ArrayList<Journey> result = testjourney.calculate();
        
        ArrayList<int[]> journeyEntries;
        int testServ = 0;
        for (Journey journey : result)
        {
          journeyEntries = journey.getEntries();
          
          for (int[] entry : journeyEntries)
          {
             testServ = entry[3];
             assertEquals(expResult, testServ);
             expResult = 6517;
          }
        }

        // TODO review the generated test code and remove the default call to fail.
    }
    
    @Test
    public void testCalculate3() throws ParseException {
        System.out.println("calculate");
        Date date = new SimpleDateFormat("yyyy/MM/dd").parse("05/05/2014");
        CalculateJourney testjourney = new CalculateJourney("Stockport, Lower Bents Lane/Stockport Road", "Thornsett, Printers Arms", 600, date);
        int expResult = 6336;
        ArrayList<Journey> result = testjourney.calculate();
        
        ArrayList<int[]> journeyEntries;
        int testServ = 0;
        for (Journey journey : result)
        {
          journeyEntries = journey.getEntries();
          
          for (int[] entry : journeyEntries)
          {
             testServ = entry[3];
             assertEquals(expResult, testServ);
             expResult = 6518;
          }
        }
    }
}